<?php

namespace App\Http\Controllers;

use App\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        return response()->json(Order::all());
    }

    function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = Validator::make($request->all(),[
            'name' => 'required',
            'email' => 'required|email',
            'phone' => 'required|regex:/^\d+$/',
            'address' => 'required',
            'content' => 'required|json'
        ]);

        if ($validation->fails()){
            return response()->json(["status"=>"NOT OK"])->setStatusCode(400);
        }

        $follow = $this->generateRandomString();

        $order = new Order();
        $order->name = $request->get('name');
        $order->email = $request->get('email');
        $order->phone = $request->get('phone');
        $order->address = $request->get('address');
        $order->content = $request->get('content');
        $order->follow = $follow;
        $order->save();

        return response()->json([
            'status' => 'OK',
            'follow' => $follow
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  string  $follow
     * @return \Illuminate\Http\Response
     */
    public function show($follow)
    {
        $order = Order::where('follow', '=', $follow)->firstOrFail();
        return response()->json($order);
    }
}
