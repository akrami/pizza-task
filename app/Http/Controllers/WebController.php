<?php

namespace App\Http\Controllers;

use App\Order;
use Illuminate\Http\Request;

class WebController extends Controller
{
    public function follow($follow) {
        $order = Order::where('follow', '=', $follow)->firstOrFail();
        return view('follow', compact('order'));
    }
}
