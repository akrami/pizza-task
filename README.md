# Pizza Task

This is a development task.

## Development Stack
- PHP - Laravel
- PostgreSQL
- ReactJS

## Deployment
Deployed on heroku under [https://calm-crag-13512.herokuapp.com/](https://calm-crag-13512.herokuapp.com/)
