<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Pizza Task</title>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/semantic-ui@2.4.2/dist/semantic.min.css" />
        <link rel="stylesheet" href="/css/app.css">

    </head>
    <body>
        <div class="ui container">
            <div class="ui secondary menu">
                <div class="item"><img src="/images/pizzatask.png"/></div>
                <div class="item header"><h1>Pizza Task</h1></div>
            </div>
            <div class="ui segment">
                <h2>Track Your Order</h2>
                <table class="ui table very basic">
                    <tbody>
                    <tr>
                        <td><b>Name</b></td>
                        <td class="right aligned">{{ $order->name }}</td>
                    </tr>
                    <tr>
                        <td><b>Email</b></td>
                        <td class="right aligned">{{ $order->email }}</td>
                    </tr>
                    <tr>
                        <td><b>Phone</b></td>
                        <td class="right aligned">{{ $order->phone }}</td>
                    </tr>
                    <tr>
                        <td><b>Tracking Code</b></td>
                        <td class="right aligned">{{ $order->follow }}</td>
                    </tr>
                    <tr>
                        <td><b>Status</b></td>
                        <td class="right aligned">{{ $order->status }}</td>
                    </tr>
                    <tr>
                        <td colspan="2"><b>Address:</b> {{ $order->address }}</td>
                    </tr>
                    </tbody>
                </table>
                <table class="ui table">
                    <thead>
                    <tr>
                        <th>Pizza</th>
                        <th>Count</th>
                        <th>Price</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach(json_decode($order->content) as $pizza)
                            <tr>
                                <td>{{ $pizza->name }}</td>
                                <td>×{{ $pizza->count }}</td>
                                <td>{{ $pizza->price }}€</td>
                            </tr>
                        @endforeach
                        <tr class="positive">
                            <td><b>Total</b></td>
                            <td colspan="2">{{ array_reduce(json_decode($order->content, true), function ($acc, $item){return $acc+($item['price']*$item['count']);}, 0) }}€</td>
                        </tr>
                    <tr class="positive">
                        <td><b>Delivery Fee</b></td>
                        <td colspan="2">5€</td>
                    </tr>
                    <tr class="negative">
                        <td><b>Payable Amount</b></td>
                        <td colspan="2"><b>{{ array_reduce(json_decode($order->content, true), function ($acc, $item){return $acc+($item['price']*$item['count']);}, 0) + 5 }}€</b> ({{ (array_reduce(json_decode($order->content, true), function ($acc, $item){return $acc+($item['price']*$item['count']);}, 0) + 5)*1.12 }}$)</td>
                    </tr>
                    </tbody>
                </table>
                <div class="ui secondary segment center aligned">
                    <p>You can track your order by visiting <a href="{{ url()->current() }}">{{ url()->current() }}</a></p>
                </div>
                <a href="/" class="ui primary button"><i class="left arrow icon"></i> Home & New Order</a>
            </div>
        </div>
    </body>
</html>
