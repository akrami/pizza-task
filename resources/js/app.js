import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import Menu from './components/Menu';
import Nav from './components/Nav';
import Cart from './components/Cart';
import {BrowserRouter, Route} from 'react-router-dom';
import axios from 'axios';

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            pizzas: [],
            cart: []
        };

        this.addToCart = this.addToCart.bind(this);
        this.removeFromCart = this.removeFromCart.bind(this);
    }

    componentWillMount() {
        axios.get('/api/pizza')
            .then(response => this.setState({pizzas: response.data}))
            .catch(errors => console.log(errors));
    }

    addToCart(id) {
        let newCart = this.state.cart;
        const index = newCart.find(key => key.id === id);
        if (index) {
            index.count = index.count + 1;
        } else {
            newCart.push({id, count: 1});
        }
        this.setState({cart: newCart});
    }

    removeFromCart(id) {
        let newCart = this.state.cart;
        const index = newCart.findIndex(key => key.id === id);
        if (index >= 0) {
            if (newCart[index].count === 1) newCart.splice(index, 1);
            else newCart[index].count = newCart[index].count - 1;
            this.setState({cart: newCart});
        }
    }

    render() {
        return (
            <div>
                <BrowserRouter>
                    <Nav cart={this.state.cart} pizzas={this.state.pizzas} addToCart={this.addToCart}
                         removeFromCart={this.removeFromCart}/>
                    <Route path="/" exact
                           render={props => <Menu pizzas={this.state.pizzas} cart={this.state.cart}
                                                  addToCart={this.addToCart} removeFromCart={this.removeFromCart}/>}/>
                    <Route path="/cart" exact
                           render={props => <Cart pizzas={this.state.pizzas} cart={this.state.cart}/>}/>
                </BrowserRouter>
            </div>
        );
    }
}

export default App;
ReactDOM.render(<App/>, document.getElementById('root'));
