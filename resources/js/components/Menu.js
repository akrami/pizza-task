import React from 'react';
import {Button, Card, Icon} from 'semantic-ui-react';

const Menu = props => {
    const {pizzas, cart, addToCart, removeFromCart} = props;

    return (
        <Card.Group centered>
            {pizzas.map(each =>
                <Card key={each.id}>
                    <Card.Content>
                        <Card.Description style={{float:"right"}}>{each.price}€</Card.Description>
                        <Card.Header>{each.name}</Card.Header>
                        <Card.Meta>{each.description}</Card.Meta>
                    </Card.Content>
                    <Card.Content extra>
                        <Button.Group fluid>
                            <Button onClick={() => removeFromCart(each.id)}
                                    negative disabled={!cart.find(item => item.id === each.id)}><Icon name="minus"/></Button>
                            <Button>{
                                cart.find(item => item.id === each.id) ? cart.find(item => item.id === each.id).count : 0
                            }</Button>
                            <Button onClick={() => addToCart(each.id)}
                                    positive><Icon name="plus"/></Button>
                        </Button.Group>
                    </Card.Content>
                </Card>)}
        </Card.Group>
    )
}

export default Menu;
