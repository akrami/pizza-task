import React, {useState} from 'react';
import {Menu, Icon, Popup, List, Label, Button} from 'semantic-ui-react';
import {Link, useLocation } from "react-router-dom";

const Nav = props => {
    const {cart, pizzas, addToCart, removeFromCart} = props;
    const [cartModal, openCart] = useState(false);
    const { pathname } = useLocation();

    const content = cart.map(each => {
        const pizza = pizzas.find(element => element.id === each.id);
        each.name = pizza.name
        each.price = pizza.price
        return each;
    });

    const popupContent = <div>
        <List celled size="big" verticalAlign='middle' style={{width: "350px"}}>
            <List.Item style={{fontSize: "24px", fontWeight: "bold", padding: "7px 13px"}}>Cart</List.Item>
            {content.length === 0 ? <List.Item style={{textAlign: "center"}}>No Pizza Selected</List.Item> :
                content.map(each => <List.Item key={each.id}>
                    <List.Content floated="right">
                        <Button.Group size="small">
                            <Button icon onClick={() => removeFromCart(each.id)}><Icon name="minus"/></Button>
                            <Button.Or text={each.count}/>
                            <Button icon onClick={() => addToCart(each.id)}><Icon name="plus"/></Button>
                        </Button.Group>
                    </List.Content>
                    <List.Content><b>{each.name}</b></List.Content>
                    <List.Description>{each.price}€</List.Description>
                </List.Item>)}
            <List.Item>
                <List.Content floated="right"><Link to="/cart"><Button primary
                                                                  disabled={content.length === 0} icon labelPosition="right">Finish<Icon name="right arrow" /></Button></Link></List.Content>
                <List.Header>Total:</List.Header>
                <List.Description>{(content.reduce((acc, each) => acc + parseFloat((each.price * each.count).toFixed(2)), 0)).toFixed(2)}€</List.Description>
            </List.Item>
        </List>
    </div>

    return (
        <Menu secondary>
            <Menu.Item><img src="/images/pizzatask.png"/></Menu.Item>
            <Menu.Item header><h1>Pizza Task</h1></Menu.Item>
            <Menu.Menu position="right">
                {pathname==='/' && <Menu.Item>
                    {content.length!==0 && <Label color='red' circular floating style={{top:"-0.4em", left:"75%"}}>{content.reduce((acc, each)=>acc+each.count, 0)}</Label>}
                    <Popup
                        style={{padding: "0"}}
                        trigger={<Icon link name='cart' size="big" style={{color:"#E8B340"}}
                                       onClick={() => openCart(!cartModal)}/>}
                        content={popupContent}
                        open={cartModal}
                        position="top right"/>
                </Menu.Item> }
            </Menu.Menu>
        </Menu>
    );
}

export default Nav;
