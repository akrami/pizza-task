import React, {useState} from 'react';
import {Button, Form, List, Segment, Table} from 'semantic-ui-react';
import {Link} from 'react-router-dom';
import axios from 'axios';

const Cart = props => {
    const {pizzas, cart} = props;
    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [phone, setPhone] = useState('');
    const [address, setAddress] = useState('');
    const [error, setError] = useState({
        name: true, email: true, phone: true, address: true
    });

    const content = cart.map(each => {
        const pizza = pizzas.find(element => element.id === each.id);
        each.name = pizza.name
        each.price = pizza.price
        return each;
    });

    const validateEmail = email => {
        const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }

    const validatePhone = phone => {
        const re = /^\d+$/;
        return re.test(String(phone));
    }

    const changeName = event => {
        setName(event.target.value);
        if (name.length > 0) setError({name: false, email: error.email, phone: error.phone, address: error.address});
        else setError({name: true, email: error.email, phone: error.phone, address: error.address});
    }

    const changeEmail = event => {
        setEmail(event.target.value);
        if (validateEmail(event.target.value)) setError({
            name: error.name,
            email: false,
            phone: error.phone,
            address: error.address
        });
        else setError({name: error.name, email: true, phone: error.phone, address: error.address});
    }

    const changePhone = event => {
        setPhone(event.target.value);
        if (validatePhone(event.target.value)) setError({
            name: error.name,
            email: error.email,
            phone: false,
            address: error.address
        });
        else setError({name: error.name, email: error.email, phone: true, address: error.address});
    }

    const changeAddress = event => {
        setAddress(event.target.value);
        if (address.length > 0) setError({name: error.name, email: error.email, phone: error.phone, address: false});
        else setError({name: error.name, email: error.email, phone: error.phone, address: true});
    }

    const submitOrder = () => {
        const validation = validatePhone(phone) && validateEmail(email) && address.length > 0 && name.length > 0;
        if (validation) {
            axios.post('/api/orders', {
                name,
                email,
                phone,
                address,
                content: JSON.stringify(content)
            })
                .then(response => window.location = `/follow/${response.data.follow}`)
                .catch(error => console.log(error));
        } else alert('Please fill all the required fields');
    }

    return (
        <div>
            <Segment>
                <h2>Finish your order</h2>
                <Form>
                    <Form.Group widths="equal">
                        <Form.Input fluid label='Name' placeholder="Name" value={name} onChange={changeName}
                                    error={error.name}/>
                        <Form.Input fluid label='Email' placeholder="Email" value={email} onChange={changeEmail}
                                    error={error.email}/>
                        <Form.Input fluid label='Phone' placeholder="Phone" value={phone} onChange={changePhone}
                                    error={error.phone}/>
                    </Form.Group>
                    <Form.TextArea label='Address' placeholder="Address..." value={address} onChange={changeAddress}
                                   error={error.address}/>
                </Form>
                <br/>
                <Table celled>
                    <Table.Header>
                        <Table.Row>
                            <Table.HeaderCell>Pizza</Table.HeaderCell>
                            <Table.HeaderCell>Count</Table.HeaderCell>
                            <Table.HeaderCell>Price</Table.HeaderCell>
                        </Table.Row>
                    </Table.Header>
                    <Table.Body>
                        {content.map(each => <Table.Row key={each.id}>
                            <Table.Cell>{each.name}</Table.Cell>
                            <Table.Cell>×{each.count}</Table.Cell>
                            <Table.Cell>{each.price}€</Table.Cell>
                        </Table.Row>)}

                        <Table.Row positive>
                            <Table.Cell>Total</Table.Cell>
                            <Table.Cell
                                colSpan="2">{(content.reduce((acc, each) => acc + parseFloat((each.price * each.count).toFixed(2)), 0)).toFixed(2)}€</Table.Cell>
                        </Table.Row>
                        <Table.Row positive>
                            <Table.Cell>Delivery Fee</Table.Cell>
                            <Table.Cell colSpan="2">5€</Table.Cell>
                        </Table.Row>
                        <Table.Row negative>
                            <Table.Cell><b>Payable Amount</b></Table.Cell>
                            <Table.Cell
                                colSpan="2"><b>{(content.reduce((acc, each) => acc + parseFloat((each.price * each.count).toFixed(2)), 0) + 5).toFixed(2)}€</b> ({((content.reduce((acc, each) => acc + parseFloat((each.price * each.count)), 0) + 5).toFixed(2)*1.12).toFixed(2)}$)</Table.Cell>
                        </Table.Row>
                    </Table.Body>
                </Table>
                <Link to="/"><Button secondary>Go Back</Button></Link>
                <Button primary onClick={submitOrder}>Pay</Button>
            </Segment>
        </div>
    );
}

export default Cart;
