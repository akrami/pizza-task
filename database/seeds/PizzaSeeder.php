<?php

use Illuminate\Database\Seeder;

class PizzaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $pizzas = [
            [
                'name' => 'Africana',
                'description' => 'chicken, banana, pineapple, onion, curry powder',
                'price' => 7.0
            ],
            [
                'name' => 'Bolognese',
                'description' => 'minced meat, onion, fresh tomato',
                'price' => 6.5
            ],
            [
                'name' => 'Calzone',
                'description' => 'ham',
                'price' => 5.0
            ],
            [
                'name' => 'Capricciosa',
                'description' => 'mushrooms, ham',
                'price' => 8.9
            ],
            [
                'name' => 'Ciao-ciao',
                'description' => 'beef, garlic, onion',
                'price' => 12.0
            ],
            [
                'name' => 'Frutti di mare',
                'description' => 'tuna, shrimp, mussels',
                'price' => 8.5
            ],
            [
                'name' => 'Hawaii',
                'description' => 'ham, pineapple',
                'price' => 7.0
            ],
            [
                'name' => 'Kebabpizza',
                'description' => 'döner kebab, onion, green peperoncini',
                'price' => 8.0
            ],
            [
                'name' => 'Marinara',
                'description' => 'shrimp, mussels',
                'price' => 13.0
            ],
            [
                'name' => 'Mexicana',
                'description' => 'minced beef, jalapeños, onion, spicy sauce',
                'price' => 12.9
            ],
            [
                'name' => 'Napolitana',
                'description' => 'anchovies, olives, capers',
                'price' => 4.9
            ],
            [
                'name' => 'Quattro Stagioni',
                'description' => 'ham, shrimp, mussels, mushrooms, artichoke',
                'price' => 12.5
            ],
            [
                'name' => 'Vegetariana',
                'description' => 'mushrooms, onion, pineapple, artichokes, asparagus, red bell pepper',
                'price' => 9.9
            ]
        ];

        foreach ($pizzas as $pizza) {
            $pizzaTemp = new \App\Pizza();
            $pizzaTemp->name = $pizza['name'];
            $pizzaTemp->description = $pizza['description'];
            $pizzaTemp->price = $pizza['price'];
            $pizzaTemp->save();
        }
    }
}
